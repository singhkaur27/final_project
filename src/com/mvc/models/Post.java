package com.mvc.models;

import java.sql.Timestamp;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
import java.util.Date;

import com.mvc.db.PostDBUtil;
import com.mvc.helpers.RandomUtil;

public class Post {

    private String postId;
    private String content;
    private String image;
    private Timestamp date;
    private String likes;
    private String email;
    
    public Post(String postId, String email, String content, String image, Timestamp date) {
		this.postId = postId;
		this.content = content;
		this.date = date;
		this.image = image;
		this.email = email;
	}
    
    public Post(String content, String image, String email) {
    	Date date = new Date();
		Timestamp sqlTime = new Timestamp(date.getTime());
		
		this.postId = String.valueOf(RandomUtil.getRandomID(11111, 99999));	
		this.content = content;
		this.date = sqlTime;
		this.image = image;
		this.email = email;
	}

	public String getPostId() {
		return this.postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getLikes() {
		return this.likes;
	}

	public void setLikes(PostDBUtil postdb) {
		try {
			this.likes  = postdb.getPostLikes(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
