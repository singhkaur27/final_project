package com.mvc.models;

import java.util.Date;

public class Message {
    private User to;
    private String content;
    private Date date;
    
	public Message(User to, String content, Date date) {
		this.to = to;
		this.content = content;
		this.date = date;
	}

	public User getTo() {
		return to;
	}

	public void setTo(User to) {
		this.to = to;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}

