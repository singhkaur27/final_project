package com.mvc.models;

import java.util.ArrayList;

import com.mvc.db.PostDBUtil;
import com.mvc.db.UserDBUtil;

public class User {
	
	private String fname;
	private String lname;
	private String email;
	private String pass;
	public String post;
	public int userid;
	private ArrayList<User> friends = new ArrayList<>();
	private ArrayList<Post> posts = new ArrayList<>();
	private ArrayList<Message> messages = new ArrayList<>();
	
	public User(String fname, String lname, String email, String pass) {
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.pass = pass;
	}	
	
	public User(String email, String pass) {
		this.email = email;
		this.pass = pass;
	}
	
	public User(String email,String post,int userid) {
		
		this.email=email;
		this.post=post;
		this.userid=userid;
		
		
	}

	public boolean register(UserDBUtil userdb) {
		try {
			userdb.insertUser(this);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean login(UserDBUtil userdb) {
		User tempUser;
		 try {
			tempUser = userdb.findUser(this.email);
						
			if(tempUser != null) {
				if(this.pass.equals(tempUser.getPass())) {
					
					this.fname = tempUser.getFname();
					this.lname = tempUser.getLname();
					this.email = tempUser.getEmail();
					this.pass = tempUser.getPass();
					
					return true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return false;
	}
	
	
	public String getpost() {
		return this.post;
	}
	
	public void post() {
	this.post=post;
	}
	

	public int getuserid() {
		return this.userid;
	}
	
	public void setuserid(int userid) {
	this.userid=userid;
	}
	
	
	
	
	public String getFname() {
		return this.fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return this.lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void createPost(String content,String image, PostDBUtil postdb) {
		
		try {
			postdb.insertPost(new Post(content,image,this.email));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editPost(String postId, PostDBUtil postdb) {
		System.out.println("edit-post");
		
		try {
			postdb.editPost(postId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deletePost(String postId, PostDBUtil postdb) {
		try {
			postdb.deletePost(postId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void likePost(String postId, PostDBUtil postdb) {
		System.out.println("like-post");
		try {
			postdb.likePost(postId,this.email);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ArrayList<User> getFriends() {
		return friends;
	}

	public void setFriends(ArrayList<User> friends) {
		this.friends = friends;
	}

	public ArrayList<Post> getPosts() {
		return this.posts;
	}

	public void setPosts(ArrayList<Post> posts) {
		this.posts = posts;
	}

	public ArrayList<Message> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}
	
	
}
