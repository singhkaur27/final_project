package com.mvc.servlets;

import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mvc.db.UserDBUtil;
import com.mvc.models.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    
    @Resource(name="jdbc/socialmedia")
	private DataSource dataSource;
	private UserDBUtil userdb;
	
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		try {
			userdb = new UserDBUtil(dataSource);			
		}catch(Exception ex) {
			throw new ServletException(ex);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		// CREATE SESSION
		HttpSession session = request.getSession();
		
		// GET DATA
		String email = request.getParameter("email");
		String pass = request.getParameter("password");
		System.out.println(email);
//		System.exit();
		// TEMP USER
		User tempUser = new User(email,pass);
		
		
		// USER FOUND ?
		boolean foundUser = tempUser.login(userdb);
		
		// SET SESSION
		if(foundUser) {
			session.setAttribute("user", tempUser);
			response.sendRedirect("home.jsp");
		}else {
//			RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
//			request.setAttribute("loginError", true);
//			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}


}

