package com.mvc.servlets;

import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;

import javax.annotation.Resource;
//import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mvc.db.PostDBUtil;
import com.mvc.models.Post;
import com.mvc.models.User;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Resource(name="jdbc/project")
   	private DataSource dataSource;
   	private PostDBUtil postdb;

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		
		try {
			postdb = new PostDBUtil(dataSource);			
		}catch(Exception ex) {
			throw new ServletException(ex);
		}
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// CREATE SESSION
		HttpSession session = request.getSession();
		
//		GET USER FROM SESSION
		User user = (User) session.getAttribute("user");
		
		try {
			//GET ALL USER POSTS
			//user.setPosts(postdb);			
			
			postdb.getUserPosts(user);
			
			//SET POST LIKES
			for(Post post:user.getPosts()) {
				post.setLikes(postdb);
			}
			
			//UPDATE SESSION
			session.setAttribute("user", user);	
		} catch (Exception e) {
			e.printStackTrace();
		}finally {		
			response.sendRedirect("profile.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

