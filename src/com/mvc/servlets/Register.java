package com.mvc.servlets;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mvc.db.UserDBUtil;
//import com.mvc.helpers.PasswordUtil;
import com.mvc.models.User;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Resource(name="jdbc/socialmedia")
	private DataSource dataSource;
	private UserDBUtil userdb;
    
	

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		try {
			userdb = new UserDBUtil(dataSource);			
		}catch(Exception ex) {
			throw new ServletException(ex);
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		// CREATE SESSION
		HttpSession session = request.getSession();
		
		//GET DATA
		String fname = request.getParameter("firstname");
		String lname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String pass = request.getParameter("password");

//		System.out.println(fname+ "first name");
		// TEMP USER
		User tempUser = new User(fname,lname,email,pass);
		
		// REGISTER USER
		boolean created =  tempUser.register(userdb);	
		
		if(created) {
			// SET SESSION
			session.setAttribute("user", tempUser);
			response.sendRedirect("Login.jsp");	
		}else{
			RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
			request.setAttribute("registerError", true);
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
