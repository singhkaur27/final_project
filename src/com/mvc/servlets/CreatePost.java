package com.mvc.servlets;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.mvc.db.PostDBUtil;
//import com.mvc.helpers.RandomUtil;
//import com.project.db.UserDBUtil;
import com.mvc.models.User;

/**
 * Servlet implementation class CreatePost
 */
@WebServlet("/CreatePost")
public class CreatePost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePost() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Resource(name="jdbc/socialmedia")
   	private DataSource dataSource;
   	private PostDBUtil postdb;
   	
   	@Override
   	public void init() throws ServletException {
   		// TODO Auto-generated method stub
   		super.init();
   		try {
   			postdb = new PostDBUtil(dataSource);			
   		}catch(Exception ex) {
   			throw new ServletException(ex);
   		}
   	}
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		String content = request.getParameter("createpost");
		
		user.createPost(content,"",postdb);
	
		response.sendRedirect("Profile");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
