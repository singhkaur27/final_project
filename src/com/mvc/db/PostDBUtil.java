package com.mvc.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Date;

import javax.sql.DataSource;

import com.mvc.models.Post;
import com.mvc.models.User;
//import com.project.models.User;

public class PostDBUtil {

	private DataSource dataSource;
	
	public PostDBUtil(DataSource dataSource) {
		this.dataSource =  dataSource;
	}
	
	// GET ALL POSTS
		public ArrayList<Post> getAllPosts() throws Exception{
			ArrayList<Post> posts = new ArrayList<>();
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			try {
				conn = this.dataSource.getConnection();
				String sql = "select * from posts";
				smt = conn.createStatement();
				res = smt.executeQuery(sql);
				
				while(res.next()) {
					String postId = res.getString("postID").toString();
					String email = res.getString("email").toString();
					String content = res.getString("content").toString();
					String image = res.getString("image").toString();
					Timestamp date = res.getTimestamp("date");
				
					//Date date = new SimpleDateFormat("dd/MM/yyyy").parse(tempDate); 
					posts.add(new Post(postId, email, content, image, date));	
				}
				return posts;
			}
			finally {
				close(conn,smt,res);
			}
		}
		
		// GET ALL POSTS
		public void getUserPosts(User user) throws Exception{
			ArrayList<Post> posts = new ArrayList<>();
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			try {
				conn = this.dataSource.getConnection();
				String sql = "select * from posts WHERE email = ?";
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, user.getEmail());
				res = pstmt.executeQuery();
				
				while(res.next()) {
					
					String email=res.getString(2);
					String post=res.getString(3);
					int userid=res.getInt(1);
					user=new User(email,post,userid);
					
					String postId = res.getString("postID").toString();
//					String email = res.getString("email").toString();
					String content = res.getString("content").toString();
					String image = res.getString("image").toString();
					Timestamp date = res.getTimestamp("date");
				
					posts.add(new Post(postId, email, content, image, date));	
				}
				user.setPosts(posts);
			}
			finally {
				close(conn,smt,res);
			}
		}
		
		//Insert One Post
		public void insertPost(Post post) throws Exception{
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			String postId = post.getPostId();
			String content = post.getContent();
			String image = post.getImage();
			String email = post.getEmail();
			Timestamp date = post.getDate();
			//System.out.println("post created 1");
			
			try {
				conn = this.dataSource.getConnection();
				
				//System.out.println("post created");
				String sql = "INSERT INTO posts VALUES(?,?,?,?,?)";
	
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, postId);
				pstmt.setString(2, email);
				pstmt.setString(3, content);
				pstmt.setString(4, image);
				pstmt.setTimestamp(5, date);
				pstmt.executeUpdate();
				
			}
			finally {
				close(conn,smt,res);
			}
		}		
		
		
//		DELETE POST
		public void deletePost(String postId) throws Exception{
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			try {
				conn = this.dataSource.getConnection();
				String sql = "DELETE FROM posts WHERE postID=?";
	
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, postId);
				pstmt.executeUpdate();
			}
			finally {
				close(conn,smt,res);
			}
		}	
		
//		EDIT POST
		public void editPost(String postId) throws Exception{
//			Connection conn = null;
//			Statement smt = null;
//			ResultSet res = null;
//			
//			String postId = post.getPostId();
//			String content = post.getContent();
//			String image = post.getImage();
//			String email = post.getEmail();
//			Timestamp date = post.getDate();
//			
//			try {
//				conn = this.dataSource.getConnection();
//				String sql = "INSERT INTO posts VALUES(?,?,?,?,?)";
//	
//				PreparedStatement pstmt = conn.prepareStatement(sql);
//				pstmt.setString(1, postId);
//				pstmt.setString(2, email);
//				pstmt.setString(3, content);
//				pstmt.setString(4, image);
//				pstmt.setTimestamp(5, date);
//				pstmt.executeUpdate();
//				return true;
//			}
//			finally {
//				close(conn,smt,res);
//			}
		}
		
//		LIKE Post
		public void likePost(String postId, String email) throws Exception{
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			try {
				conn = this.dataSource.getConnection();
				String sql = "SELECT email FROM likes WHERE postID=? AND email=?";
	
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, postId);
				pstmt.setString(2, email);
				res = pstmt.executeQuery();
				res.next();
				
				if(res.wasNull()) {
					sql = String.format("INSERT INTO likes VALUES('%s','%s')",postId,email);
					Statement stm = conn.createStatement();
					stm.executeUpdate(sql);
					System.out.println("liked");
				}else {
					sql = "DELETE FROM likes WHERE postID=? AND email=?";
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, postId);
					pstmt.setString(2, email);
					pstmt.executeUpdate();
					System.out.println("unliked");
//					return false;
				}
				
			}
			finally {
				close(conn,smt,res);
			}
		}
		
		
//		GET POST LIKES
		public String getPostLikes(Post post) throws Exception{
			Connection conn = null;
			Statement smt = null;
			ResultSet res = null;
			
			try {
				conn = this.dataSource.getConnection();
				String sql = "SELECT count(postID) as 'likes' from likes WHERE postID=?";
	
				PreparedStatement pstmt = conn.prepareStatement(sql);
				pstmt.setString(1, post.getPostId());
				res = pstmt.executeQuery();
				res.next();
				return res.getString("likes");
			}
			finally {
				close(conn,smt,res);
			}
		}	
				
		private void close(Connection conn, Statement smt, ResultSet res) {
			try {
				if(res != null) {
					res.close();
				}
				if(smt != null) {
					smt.close();
				}
				if(conn != null) {
					conn.close();
				}
			}
			catch(Exception exe) {
				exe.printStackTrace();
			}
		}
}

