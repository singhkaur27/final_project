package com.mvc.db;
import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sql.DataSource;
import com.mvc.models.User;

public class UserDBUtil {
	
	private DataSource dataSource;
	
	public UserDBUtil(DataSource dataSource) {
		this.dataSource =  dataSource;
	}
	
	// FIND USER
	public User findUser(String email) throws Exception{
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		User foundUser = null;
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "SELECT * from user WHERE Email = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			res = pstmt.executeQuery();
			
			res.next();
			String tempFname = res.getString("FirstName").toString();
			String tempLname =res.getString("LastName").toString();
			String tempEmail = res.getString("Email").toString();
			String tempPass = res.getString("Password").toString();
			System.out.println(tempFname);
			foundUser = new User(tempFname,tempLname,tempEmail,tempPass);
			return foundUser;
		
		}
		finally {
			close(conn,smt,res);
		}
	}
	
	// INSERT USER
	public boolean insertUser(User user) throws Exception{
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		
		String fname = user.getFname();
		String lname = user.getLname();
		String email = user.getEmail();
		String pass = user.getPass();
		
		try {
			conn = this.dataSource.getConnection();
			String sql = String.format("INSERT INTO user VALUES('%s','%s','%s','%s','')",fname,lname,email,pass);
			smt = conn.createStatement();
			smt.executeUpdate(sql);
		}
		finally {
			close(conn,smt,res);
		}
		
		return true;
	}
	
	// GET ALL USERS
	public ArrayList<User> getAllUsers() throws Exception{
		ArrayList<User> users = new ArrayList<>();
		Connection conn = null;
		Statement smt = null;
		ResultSet res = null;
		
		try {
			conn = this.dataSource.getConnection();
			String sql = "select * from user";
			smt = conn.createStatement();
			res = smt.executeQuery(sql);
			
			while(res.next()) {
				String fname = res.getString("FirstName").toString();
				String lname = res.getString("LastName").toString();
				String pass = res.getString("Password").toString();
				String email = res.getString("Email").toString();
		
				users.add(new User(fname,lname,email,pass));	
			}
			return users;
		}
		finally {
			close(conn,smt,res);
		}
	}
	
	private void close(Connection conn, Statement smt, ResultSet res) {
		try {
			if(res != null) {
				res.close();
			}
			if(smt != null) {
				smt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		catch(Exception exe) {
			exe.printStackTrace();
		}
	}
	
}
